/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.mozaic001;

import java.awt.Color;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import cc.plural.display.Graphics;
import cc.plural.display.GraphicsManager;
import cc.plural.mozaic001.GameState.Logo;
import cc.plural.timing.ScheduledActionExecuteException;
import cc.plural.timing.TimedAction;
import cc.plural.timing.Timer;

public class RenderAction extends TimedAction {

	final NumberFormat numberFormat;
	
	GameState state;

	boolean bufferConfigured;

	TimedAction displayAction;
	TimedAction gameStateAction;
	
	
	long lastTime = 0L;

	long display = 0L;

	String displaySpeedString = "";

	String line1 = "";

	String line2 = "";

	public RenderAction(GameState state, TimedAction displayAction, TimedAction gameStateAction) {
		this.state = state;
		this.displayAction = displayAction;
		this.gameStateAction = gameStateAction;
		this.bufferConfigured = false;
		this.numberFormat = new DecimalFormat("#00.00");
	}

	protected void handleExecute(long time, long lag) throws ScheduledActionExecuteException {
		if (!this.state.getStateCheck().valid()) {
			return;
		}
		if (!this.bufferConfigured) {
			this.state.getDisplay().getGraphicsManager().init();
			this.bufferConfigured = true;
		}

		GraphicsManager manager = state.getDisplay().getGraphicsManager();

		Graphics graphics = manager.getDrawGraphics();

		if (graphics == null) {
			System.out.println("Graphics Null.. Skipping.");
			return;
		}

		synchronized (graphics) {
			List<Logo> logos = state.getLogos();
			graphics.setColor(Color.BLACK);
			graphics.fillRect(0, 0, manager.getGraphicsWidth(), manager.getGraphicsHeight());

			for (Logo logo : logos) {
				int x = (int) logo.getX();
				int y = (int) logo.getY();

				graphics.setColor(logo.color);
				graphics.drawString(logo.getText(), x, y);
			}

			graphics.setColor(Color.LIGHT_GRAY);
			graphics.fillRect(0, 0, 550, 40);
			graphics.setColor(Color.BLACK);
			graphics.drawRectangle(0, 0, 549, 40);
			graphics.setColor(Color.WHITE);
			graphics.fillRect(0, 0, 550, 20);
			graphics.setColor(Color.BLACK);
			graphics.drawRectangle(0, 0, 549, 20);
			graphics.setColor(Color.BLUE);
			graphics.drawString("Mozaic Test 001 - Render", 2, 15);

			if (lastTime == 0L) {
				display = 0L;
				lastTime = time;

				line1 = (displaySpeedString + "hz rendering " + state.logos.size() + " objects.");
			} else if (time - lastTime > 100L) {
				display = displayAction.getAvgPayloadPeriodTimeNano();
				displaySpeedString = numberFormat.format(1000.0D / Timer.nanoSecondsToReadableMilliSeconds(display));

				lastTime = time;
				line1 = (this.displaySpeedString + "hz rendering " + state.logos.size() + " objects.");
				line2 = ("Scheduler Periods - Display: " + numberFormat.format(Timer.nanoSecondsToReadableMilliSeconds(displayAction.getAvgPayloadDeltaTimeNano()))
						+ " ms  Buffer:" + numberFormat.format(Timer.nanoSecondsToReadableMilliSeconds(getAvgPayloadDeltaTimeNano())) + " ms  GameState:"
						+ numberFormat.format(Timer.nanoSecondsToReadableMilliSeconds(gameStateAction.getAvgPayloadDeltaTimeNano())) + "ms");
			}

			graphics.setColor(Color.DARK_GRAY);
			graphics.drawString(this.line1, 180, 15);
			graphics.setColor(Color.BLACK);
			graphics.drawString(this.line2, 2, 35);
		}
	}
}