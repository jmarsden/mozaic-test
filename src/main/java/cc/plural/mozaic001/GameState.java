/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.mozaic001;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import cc.plural.display.GraphicsManager;
import cc.plural.state.State;

public class GameState extends State {
	
	static final String HEADER = "Mozaic Test 001 - Render";

	static final int MAX_LOGOS = 10000;
	
	List<Logo> logos;

	public GameState() {
		this.logos = new ArrayList<Logo>();
	}

	public synchronized void addLogo(String text, Color color, float x, float y, boolean xDirection, boolean yDirection, float speed) {
		if (this.logos.size() >= 3000) {
			this.logos = new ArrayList<Logo>();
		}
		this.logos.add(new Logo(text, color, x, y, xDirection, yDirection, speed));
	}

	public List<Logo> getLogos() {
		return this.logos;
	}
	
	@Override
	public void init() {
		
	}

	public synchronized void tick() {
		GraphicsManager manager = getDisplay().getGraphicsManager();
		int width = manager.getGraphicsWidth();
		int height = manager.getGraphicsHeight();
		for (Logo logo : this.logos)
			logo.tick(width, height);
	}

	public static class Logo {

		Color color;

		String text;

		float x;

		float y;

		float speed;

		boolean xDirection;

		boolean yDirection;

		public Logo(String text, Color color, float x, float y, boolean xDirection, boolean yDirection, float speed) {
			this.text = text;
			this.color = color;
			this.x = x;
			this.y = y;
			this.xDirection = xDirection;
			this.yDirection = yDirection;
			this.speed = speed;
		}

		public String getText() {
			return this.text;
		}

		public Color getColor() {
			return this.color;
		}

		public void tick(int graphicsWidth, int graphicsHeight) {
			if (this.xDirection) {
				this.x += this.speed;
				if (this.x >= graphicsWidth) {
					this.x = (graphicsWidth - this.speed);
					this.xDirection = false;
				}
			} else {
				this.x -= this.speed;
				if (this.x < 0.0F) {
					this.x = (0.0F + this.speed);
					this.xDirection = true;
				}
			}
			if (this.yDirection) {
				this.y += this.speed;
				if (this.y >= graphicsHeight) {
					this.y = (graphicsHeight - this.speed);
					this.yDirection = false;
				}
			} else {
				this.y -= this.speed;
				if (this.y < 0.0F) {
					this.y = (0.0F + this.speed);
					this.yDirection = true;
				}
			}
		}

		public float getX() {
			return this.x;
		}

		public float getY() {
			return this.y;
		}
	}
}