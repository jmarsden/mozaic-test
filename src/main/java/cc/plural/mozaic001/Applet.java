/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.mozaic001;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import cc.plural.display.CanvasGraphics;
import cc.plural.display.CanvasGraphicsManager;
import cc.plural.display.DisplayAction;
import cc.plural.display.GraphicsManager;
import cc.plural.state.StateCheckCase;
import cc.plural.timing.DirectAction;
import cc.plural.timing.ScheduledActionExecuteException;
import cc.plural.timing.Scheduler;
import cc.plural.timing.TimedAction;
import cc.plural.timing.Timer;

public class Applet extends java.applet.Applet implements StateCheckCase {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9063005958778228782L;

	GameState state;

	boolean displayCanvasReady;

	CanvasGraphics displayCanvas;
	
	public Applet() {
		System.out.println("Constructor");
		this.displayCanvasReady = false;
	}
	
	public void addNotify() {
		super.addNotify();
		this.displayCanvasReady = true;
	}
	
	public boolean valid() {
		return this.displayCanvasReady;
	}
	
	public void reset() {
		this.displayCanvasReady = false;
	}
	
	public void init() {
		super.init();
		System.out.println("init");
		this.state = new GameState();
		this.state.initStateCheck();
		this.state.initDisplay();

		this.displayCanvas = new CanvasGraphics();
		Dimension dimension = new Dimension(500, 500);

		this.displayCanvas.setMinimumSize(dimension);
		this.displayCanvas.setBackground(Color.BLACK);

		GraphicsManager graphicsManager = new CanvasGraphicsManager(this.displayCanvas);
		this.displayCanvas.addComponentListener(graphicsManager);
		this.state.getDisplay().registerGraphicsManager(graphicsManager, true);

		setLayout(new BorderLayout());
		add(displayCanvas, BorderLayout.CENTER);
		
		state.getStateCheck().addTest(this);
		state.getStateCheck().addTest(this.displayCanvas);
	}

	public void start() {
		super.start();
		final TimedAction gameStateAction = new GameStateAction(state);
		final TimedAction displayAction = new DisplayAction(state);
		final TimedAction renderAction = new RenderAction(state, displayAction, gameStateAction);

		Scheduler.instance.scheduleRepeatingAction(gameStateAction, 10);
		Scheduler.instance.scheduleRepeatingAction(displayAction, 10);
		Scheduler.instance.scheduleRepeatingAction(renderAction, 10);

		Scheduler.instance.scheduleRepeatingAction(new DirectAction() {
			protected void handleExecute(long time, long lag) throws ScheduledActionExecuteException {
				System.out.println("Display:" + Timer.nanoSecondsToMilliSeconds(displayAction.getAvgPayloadDeltaTimeNano()) + "ms Buffer:"
						+ Timer.nanoSecondsToMilliSeconds(renderAction.getAvgPayloadDeltaTimeNano()) + "ms GameState:"
						+ Timer.nanoSecondsToMilliSeconds(gameStateAction.getAvgPayloadDeltaTimeNano()) + "ms");
			}
		}, 1000);
	}

	public void destroy() {
		super.destroy();
		
		Scheduler.instance.reset();
		state.getDisplay().reset();
		
		remove(displayCanvas);
		System.out.println("Destroy");
	}
	
	public void stop() {
		super.stop();
	}
}
