/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.mozaic003;

import java.awt.event.ComponentEvent;
import java.awt.geom.Point2D;

import cc.plural.display.GraphicsManager;
import cc.plural.math.MersenneTwisterFast;
import cc.plural.state.State;
import cc.plural.state.StateCheckCase;
import cc.plural.tile.Portal;

public class GameState extends State implements StateCheckCase {
	
	static final String HEADER = "Mozaic Test 003 - Infinity and Beyond";
	static final String TAG_LINE = "Status INPRG";
	
	boolean ready;
	
	final MersenneTwisterFast random;
	
	GameWorld world;
	
	Portal portal;
	
	int tickCount;
	
	
	float focusX = 0;
	float focusY = 0;
	
	
	public GameState() {
		this.random = new MersenneTwisterFast(1337);
		world = null;
		portal = null;
		ready = false;
		tickCount = 0;
	}
	
	public boolean valid() {
		return ready;
	}
	
	public void reset() {
		ready = false;
		this.portal = null;
	}
	
	@Override
	public void init() {
		GameWorld world = new GameWorld(100, 100, 20, 0, 0);
		this.world = world;
	}

	@Override
	public void tick() {
	
		if(this.portal == null) {
			GraphicsManager gm = getDisplay().getGraphicsManager();
			int width = (int) gm.getGraphicsWidth();
			int height = (int) gm.getGraphicsHeight();
			this.portal = new Portal(world, width, height);
			focusX = (int) (Math.random() * width);
			focusY = (int) (Math.random() * height);
		}
		
		Point2D.Float focus = new Point2D.Float(focusX,focusY);
		this.portal.setWorldFocus(focus);
		
		
		focusX+=2;
		focusY+=2;
	}
	
	public void componentResized(ComponentEvent e) {
		GraphicsManager gm = getDisplay().getGraphicsManager();
		int width = (int) gm.getGraphicsWidth();
		int height = (int) gm.getGraphicsHeight();
		this.portal = new Portal(world, width, height);
	}
}
