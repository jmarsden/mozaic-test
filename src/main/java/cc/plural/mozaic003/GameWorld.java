/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.mozaic003;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.util.HashMap;

import cc.plural.tile.DynamicWorld;
import cc.plural.tile.ImageTile;
import cc.plural.tile.Tile;

public class GameWorld extends DynamicWorld {

	HashMap<String, Tile> tileBuffer = new HashMap<String, Tile>();
	
	public GameWorld(int tileWidth, int tileHeight, int loadWindowSize, int xInterest, int yInterest) {
		super(tileWidth, tileHeight, loadWindowSize, xInterest, yInterest);
	}

	public GameWorld(int tileWidth, int tileHeight, int loadWindowSize) {
		super(tileWidth, tileHeight, loadWindowSize);
	}

	public GameWorld(int tileWidth, int tileHeight) {
		super(tileWidth, tileHeight);
	}

	@Override
	public Tile loadTile(int x, int y) {
		String key = x + "x" + y;
		if(tileBuffer.containsKey(key)) {
			return tileBuffer.get(key);
		} else {
			if(tileBuffer.size() >= 1000) {
				System.out.println("Dumping Tile Buffer so we dont run this puppy out of memory!");
				tileBuffer = new HashMap<String, Tile>();
			}
			
			BufferedImage imageBase =  null;	
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			GraphicsDevice gs = ge.getDefaultScreenDevice();
			GraphicsConfiguration gc = gs.getDefaultConfiguration();
			imageBase = gc.createCompatibleImage(getTileHeight(), getTileWidth(), Transparency.BITMASK);
			Graphics graphics = imageBase.getGraphics();
			graphics.setColor(Color.RED);
			graphics.drawRect(0, 0, getTileHeight(), getTileWidth());
			graphics.drawString(key, 2, 12);
			ImageTile imageTile = new ImageTile("Rah", getTileHeight(), getTileWidth(), imageBase);
			tileBuffer.put(key, imageTile);
			return imageTile;
		}
		
	}

}
