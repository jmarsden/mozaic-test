/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.mozaic003;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.JFrame;

import cc.plural.display.CanvasGraphics;
import cc.plural.display.CanvasGraphicsManager;
import cc.plural.display.DisplayAction;
import cc.plural.display.GraphicsManager;
import cc.plural.state.StateCheckCase;
import cc.plural.timing.DirectAction;
import cc.plural.timing.ScheduledActionExecuteException;
import cc.plural.timing.Scheduler;
import cc.plural.timing.TimedAction;
import cc.plural.timing.Timer;

public class Application extends JFrame implements StateCheckCase {

	CanvasGraphics displayCanvas;

	GameState state;

	final NumberFormat numberFormat;

	boolean displayCanvasReady;

	private static final long serialVersionUID = 9025088486426744677L;

	protected DisplayWindowListener windowListener;

	public Application() {
		this.numberFormat = new DecimalFormat("#00.00");
	}

	public void addNotify() {
		super.addNotify();
		this.displayCanvasReady = true;
	}

	public boolean valid() {
		return this.displayCanvasReady;
	}
	
	public void reset() {
		this.displayCanvasReady = false;
	}

	public void init() {
		this.state = new GameState();
		this.state.init();
		this.state.initStateCheck();
		this.state.initDisplay();

		this.displayCanvas = new CanvasGraphics();
		Dimension dimension = new Dimension(800, 500);

		this.displayCanvas.setMinimumSize(dimension);
		this.displayCanvas.setBackground(Color.BLACK);

		GraphicsManager graphicsManager = new CanvasGraphicsManager(this.displayCanvas);
		this.displayCanvas.addComponentListener(graphicsManager);
		this.displayCanvas.addComponentListener(state);
		
		this.state.getDisplay().registerGraphicsManager(graphicsManager, true);

		setTitle("Mozaic");
		setLayout(new BorderLayout());
		setPreferredSize(dimension);
		pack();
		setLocationRelativeTo(null);
		getContentPane().add(this.displayCanvas, "Center");

		this.windowListener = new DisplayWindowListener();
		addWindowListener(this.windowListener);

		state.getStateCheck().addTest(this);
		state.getStateCheck().addTest(this.displayCanvas);
	}

	public void start() {
		final TimedAction gameStateAction = new GameStateAction(state);
		gameStateAction.setPriority(10);
		final TimedAction displayAction = new DisplayAction(state);
		displayAction.setPriority(10);
		final TimedAction renderAction = new RenderAction(state, displayAction, gameStateAction);

		Scheduler.instance.scheduleRepeatingAction(gameStateAction, 15);
		Scheduler.instance.scheduleRepeatingAction(displayAction, 15);
		Scheduler.instance.scheduleRepeatingAction(renderAction, 15);

		Scheduler.instance.scheduleRepeatingAction(new DirectAction() {
			protected void handleExecute(long time, long lag) throws ScheduledActionExecuteException {
				System.out.println("Display:" + numberFormat.format(Timer.nanoSecondsToReadableMilliSeconds(displayAction.getAvgPayloadDeltaTimeNano())) + "ms Buffer:"
						+ numberFormat.format(Timer.nanoSecondsToReadableMilliSeconds(renderAction.getAvgPayloadDeltaTimeNano())) + "ms GameState:"
						+ numberFormat.format(Timer.nanoSecondsToReadableMilliSeconds(gameStateAction.getAvgPayloadDeltaTimeNano())) + "ms");
			}
		}, 10000);
	}

	public static void main(String[] args) {
		Application instance = new Application();
		instance.init();
		instance.setVisible(true);
		instance.start();
	}

	private class DisplayWindowListener extends WindowAdapter {

		private DisplayWindowListener() {
		}

		public void windowActivated(WindowEvent e) {
			System.out.println(e.toString());

			super.windowActivated(e);
		}

		public void windowClosed(WindowEvent e) {
			System.out.println(e.toString());
			super.windowClosed(e);
		}

		public void windowClosing(WindowEvent e) {
			System.out.println(e.toString());
			super.windowClosing(e);
			System.exit(0);
		}

		public void windowDeactivated(WindowEvent e) {
			super.windowDeactivated(e);
		}

		public void windowDeiconified(WindowEvent e) {
			System.out.println(e.toString());
			super.windowDeiconified(e);
		}

		public void windowIconified(WindowEvent e) {
			System.out.println(e.toString());
			super.windowIconified(e);
		}

		public void windowOpened(WindowEvent e) {
			System.out.println(e.toString());
			super.windowOpened(e);
		}

		public void windowStateChanged(WindowEvent e) {
			System.out.println(e.toString());
			super.windowStateChanged(e);
		}
	}
}