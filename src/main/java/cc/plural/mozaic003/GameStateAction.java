package cc.plural.mozaic003;

import cc.plural.timing.ScheduledActionExecuteException;
import cc.plural.timing.TimedAction;

public class GameStateAction extends TimedAction {
	
	GameState state;

	public GameStateAction(GameState state) {
		this.state = state;
	}
	
	protected void handleExecute(long time, long lag) throws ScheduledActionExecuteException {
		if (!this.state.getStateCheck().valid()) {
			return;
		}
		state.tick();
	}
}
