/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.mozaic002;

import cc.plural.math.MersenneTwisterFast;
import cc.plural.timing.ScheduledActionExecuteException;
import cc.plural.timing.TimedAction;

public class GameStateAction extends TimedAction {
	
	GameState state;
	
	final MersenneTwisterFast random;
	
	String[] logoText = { "Mozaic", "Mozaic", "Mozaic", "Mozaic", "Test 001 V2", "plural.cc" };

	long lastTime = 0L;

	public GameStateAction(GameState state) {
		this.state = state;
		this.random = new MersenneTwisterFast(1337);
	}
	
	protected void handleExecute(long time, long lag) throws ScheduledActionExecuteException {
		if (!this.state.getStateCheck().valid()) {
			return;
		}
		state.tick();
	}
}
