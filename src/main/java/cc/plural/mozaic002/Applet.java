/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.mozaic002;

import java.awt.BorderLayout;
import java.awt.Color;

import cc.plural.display.CanvasGraphics;
import cc.plural.display.CanvasGraphicsManager;
import cc.plural.display.DisplayAction;
import cc.plural.display.GraphicsManager;
import cc.plural.state.StateCheckCase;
import cc.plural.timing.Scheduler;
import cc.plural.timing.TimedAction;

public class Applet extends java.applet.Applet implements StateCheckCase {

	GameState state;

	boolean displayCanvasReady;

	private static final long serialVersionUID = -2690493819130082326L;

	CanvasGraphics displayCanvas;

	public Applet() {
		System.out.println("Constructor");
		this.displayCanvasReady = false;
	}

	public void init() {
		System.out.println("Init");
		
		super.init();

		this.state = new GameState();
		this.state.init();
		this.state.initStateCheck();
		this.state.initDisplay();

		System.out.println("init");
		this.displayCanvas = new CanvasGraphics();
		this.displayCanvas.setBackground(Color.BLACK);

		GraphicsManager graphicsManager = new CanvasGraphicsManager(this.displayCanvas);
		this.displayCanvas.addComponentListener(graphicsManager);
		this.displayCanvas.addComponentListener(state);
		this.state.getDisplay().registerGraphicsManager(graphicsManager, true);

		setLayout(new BorderLayout());
		add(this.displayCanvas, "Center");

		this.state.getStateCheck().addTest(this);
		this.state.getStateCheck().addTest(this.displayCanvas);

	}

	public void addNotify() {
		super.addNotify();
		this.displayCanvasReady = true;
	}

	public void start() {
		super.start();

		final TimedAction gameStateAction = new GameStateAction(state);
		gameStateAction.setPriority(10);
		final TimedAction displayAction = new DisplayAction(state);
		displayAction.setPriority(10);
		final TimedAction renderAction = new RenderAction(state, displayAction, gameStateAction);
		
		Scheduler.instance.scheduleRepeatingAction(gameStateAction, 15);
		Scheduler.instance.scheduleRepeatingAction(displayAction, 15);
		Scheduler.instance.scheduleRepeatingAction(renderAction, 15);

	}

	public void destroy() {
		super.destroy();
		remove(this.displayCanvas);
		this.state.reset();
		//this.state.resetStateCheck();
		this.state.resetDisplay();
		
		Scheduler.instance.reset();
		
		System.out.println("Destroy");
	}

	public boolean valid() {
		return this.displayCanvasReady;
	}
	
	public void reset() {
		this.displayCanvasReady = false;
	}
}