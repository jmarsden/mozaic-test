/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.mozaic002;

import java.awt.event.ComponentEvent;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

import cc.plural.display.GraphicsManager;
import cc.plural.resource.ResourceException;
import cc.plural.resource.SpriteSheet;
import cc.plural.state.State;
import cc.plural.state.StateCheckCase;
import cc.plural.tile.FixedWorld;
import cc.plural.tile.ImageTile;
import cc.plural.tile.Portal;

public class GameState extends State implements StateCheckCase {
	
	static final String HEADER = "Mozaic Test 002 - Tiles";
	static final String TAG_LINE = "1 bug to fix!";
	
	boolean ready;
	
	FixedWorld world;
	
	Portal portal;
	
	int tickCount;
	int focusX;
	int focusY;
	
	boolean xDirection;
	boolean yDirection;
	
	float speed;
	
	public GameState() {
		world = null;
		portal = null;
		ready = false;
		tickCount = 0;
		focusX = 0;
		focusY = 0;
		xDirection = true;
		yDirection = true;
		speed = 8;
	}
	
	public boolean valid() {
		return ready;
	}
	
	public void reset() {
		ready = false;
		this.portal = null;
	}
	
	@Override
	public void init() {
		SpriteSheet spriteSheet = null;
        try {
                spriteSheet = new SpriteSheet("img01.jpg");
        } catch (ResourceException e) {
                e.printStackTrace();
        }
        int sheetWidth = spriteSheet.getSheetWidth();
        int sheetHeight = spriteSheet.getSheetHeight();
        int tileWidth = 40;
        int tileHeight = 40;
        
        int tileCountM = (int) ((float) sheetWidth / (float) tileWidth);
        int tileCountN = (int) ((float) sheetHeight / (float) tileHeight);

        System.out.println(String.format("Sheet Width: %s TileWidth: %s TileCountM: %s", sheetWidth, tileWidth, tileCountM));
        System.out.println(String.format("Sheet Height: %s TileHeight: %s TileCountN: %s", sheetHeight, tileHeight, tileCountN));
        
        FixedWorld world = new FixedWorld(tileCountM, tileCountN, tileWidth, tileHeight);

        BufferedImage tileImage = null;

        int x = 0;
        int y = 0;
        int c = 0;
        for (int i = 0; i < tileCountM; i++) {
                y = 0;
                for (int j = 0; j < tileCountN; j++) {
                        int tileX = i * tileWidth;
                        int tileY = j * tileHeight;
                        tileImage = spriteSheet.getImage(tileX, tileY, tileWidth, tileHeight, false, "" + i + "x" + j);
                        world.setTile(x, y, new ImageTile("tile" + i + "-" + j + "", tileWidth, tileHeight, tileImage));
                        c++;
                        y++;
                }
                x++;
        }

        System.out.println(String.format("Loaded %s Tiles.", c));
        
        this.world = world;
        this.ready = true;
	}

	@Override
	public void tick() {
		if(this.portal == null) {
			GraphicsManager gm = getDisplay().getGraphicsManager();
			int width = (int) Math.min(world.getWorldWidth(),gm.getGraphicsWidth());
			int height = (int) Math.min(world.getWorldHeight(),gm.getGraphicsHeight());
			this.portal = new Portal(world, width, height);
			focusX = (int) (Math.random() * width);
			focusY = (int) (Math.random() * height);
		}
		
		if(xDirection) {
			focusX+=speed;
		} else {
			focusX-=speed;
		}
		if(yDirection) {
			focusY+=speed;
		} else {
			focusY-=speed;
		}
		
		if(focusX <= 0) {
			focusX = (int) 0;
			xDirection = true;
		}
		if(focusX >= world.getWorldWidth()) {
			focusX = (int) world.getWorldWidth();
			xDirection = false;
		}		
		if(focusY <= 0) {
			focusY = (int) 0;
			yDirection = true;
		}
		if(focusY >= world.getWorldHeight()) {
			focusY = (int) world.getWorldHeight();
			yDirection = false;
		}
	
		Point2D.Float focus = new Point2D.Float(focusX,focusY);
		this.portal.setWorldFocus(focus);
		tickCount++;
	}
	
	public void componentResized(ComponentEvent e) {
		GraphicsManager gm = getDisplay().getGraphicsManager();
		int width = (int) Math.min(world.getWorldWidth(),gm.getGraphicsWidth());
		int height = (int) Math.min(world.getWorldHeight(),gm.getGraphicsHeight());
		this.portal = new Portal(world, width, height);
	}
}
